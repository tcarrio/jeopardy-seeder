package main

import (
	seeder "gitlab.com/tcarrio/jeopardy-seeder/pkg"
)

func main() {
	seeder.Start()
}
